-module(simple_list).
-compile(export_all).

merge_list_by_plus()->
  L1 = [a,b,c],
  L2 = [b,c],
  L1 ++ L2.

minus_list()->
  L1 = [a,b,c],
  L2 = [b,c],
  L1 -- L2.

minus_three_list()->
	L1 = [a,b,c],
    L2 = [b,c],
	L3 = [d,e],
	L1 -- L2 --L3.
head()->
  L1 = [a,b,c],
  L2 = [b,c],
  io:fwrite(hd(L1)),
  io:fwrite("\n_").
tail()->
  L1 = [a,b,c],
  L2 = [b,c],
  io:fwrite(tl(L2)),
  io:fwrite("\n_").

add_head_lists()->
	L1 = [a,b,c],
  	[aa|L1].
get_head_tail()->
  L1 = ["a","b","c"],
  [H|T] = L1,
  
  io:fwrite(string:concat("Head first ",H)),
  io:fwrite("\n"),
  io:fwrite(string:concat("tail first ",T)),
  io:fwrite("\n").

manual_recurstion()->
	L1 = ["a","b","c"],
	[H|T] = L1,
	io:fwrite(string:concat("FIRST head ",H)),
	[H1|T1] = T,
	io:fwrite("\n"),
	io:fwrite(string:concat("2nd Head ",H1)),
	[H2|T2] = T1,
	io:fwrite("\n"),
	io:fwrite(string:concat("3rd Head ",H2)),
    io:fwrite("\n").

%getting recursions
sum(L) -> sum(L, 0).
sum([H|T], Sum) -> 
	T1  = sum(T, Sum + H);
	sum([], Sum)    -> Sum.

minus(L) -> minus(L,0).

minus([H|T], Minus) -> 	
	io:format(Minus),
    if 
		Minus==0 ->  
		 	minus(T, Minus + H);
		length(T)==0 ->
			Minus - H;
%% 		Minus > 0 ->
%% 		  minus(T, Minus - H);
		true -> 
			minus(T, Minus - H)
	end.
minus_from_first(L) -> minus_from_first(L,0).
minus_from_first([H|T], Minus) -> 
	if 
		Minus==0 -> minus_from_first(T, Minus + H);
		length(T)==0 ->Minus - H; 
		true -> minus_from_first(T, Minus - H) 
	end.

%minus_from_first1(L) -> minus_from_first(L,0).
minus_from_first1([]) -> 
	0;
	minus_from_first1([H|T])->
		H-minus_from_first1(T).
	%



%minus_from_first2([1,2,3])
minus_from_first2(L) -> minus_from_first2(L,0).
minus_from_first2([],Acc)-> 
	Acc; % it will return
	minus_from_first2([H|T], 0) ->
	   +H; 
	minus_from_first2([H|T], Acc) ->
	   minus_from_first2(T,H) -  Acc . 


%
tail_reverse(L) -> tail_reverse(L,[]).
 
tail_reverse([],Acc) -> Acc;
	tail_reverse([H|T],Acc) -> tail_reverse(T, [H|Acc]).

print_list(L) -> print_list(L,0).

print_list([],Acc)-> 
	Acc;
	print_list([H|T], Acc) ->
	   io:fwrite("~w~n",[H]), 
	   print_list(T,H). 

%my factorial with resursion
my_fact(N)-> my_fact(N,N-1).
my_fact(N,R) -> 
	if 
		R > 0 -> 
			R*my_fact(N,R-1); 
		true -> 
			N
	end.
%
p_list(L)-> p_list(L,0).
p_list([],Acc)-> 
  Acc;
  p_list([H|T], Acc) ->
	 io:fwrite("~w~n",[H]),
	 io:fwrite("~w~n",[Acc]),
	 io:fwrite("---------\n"),
	 p_list(T,H). 

reverse([]) -> [];
	reverse([H | T]) -> 
		reverse(T) ++ [H].

% L means LIST
% H = HEAD

	
	
my_sum([])-> 
	0;
	my_sum([H|T])->
		my_sum(T)+ H.

my_subtract([])-> 0;
	my_subtract([H|T])->
		my_subtract(T)- H.


sublist(_,0) -> [];
sublist([],_) -> 
	[];
	sublist([H|T],N) when N > 0 -> [H|sublist(T,N-1)].

increment([]) -> [];
 increment([H|T]) -> 
	[H+1] ++ increment(T).

increment_tail(L) -> increment_tail(L,[]).
increment_tail([],Tail)-> 
	Tail;
increment_tail([H|T],Tail) -> 
     increment_tail(T,Tail++[H+1]).
%
increment_without_recursion(L)->
	[H+1 || H <- L].
% utitlity methods
incr(X) -> X + 1.
decr(X) -> X - 1.

map(_, []) -> [];
map(F, [H|T]) -> [F(H)|map(F,T)].

perform_map(L)->
	map(fun decr/1, L).
perform_math_map(L)-> 
	map(fun(X)-> math:pow(X,2) end,L ).

filter_list([],Func)->
	[];
	filter_list([H|T],Func) ->
		case Func(H) of 
			true -> 
				[H | filter_list(T,Func) ];
			false ->
			  filter_list(T,Func)
		end.
	
call_filter_list(L)->
	% get even no
	filter_list(L,fun(X)-> X rem 2 == 0 end ).

filter_list_tail(L,Func)-> filter_list_tail(L,Func,[]).
filter_list_tail([],Func,Tail)->
	Tail;
	filter_list_tail([H|T],Func,Tail) ->
		case Func(H) of 
			true -> 
			    filter_list_tail(T,Func,Tail++[H]);
			false ->
			  filter_list_tail(T,Func,Tail)
		end.
call_filter_list_tail(L)->
	% get even no
	filter_list_tail(L,fun(X)-> X rem 2 == 0 end ).

max_tail([H|T]) -> max_tail(T, H).
max_tail([], Max) -> 
	Max;
	max_tail([H|T], Max) when H > Max -> max_tail(T, H);
	max_tail([_|T], Max) -> max_tail(T, Max).



rpn(L) when is_list(L) ->
	
	[Res] = lists:foldl(fun rpn/2, [], string:tokens(L, " ")),
	Res.
rpn(X, Stack) -> [read(X)|Stack].	  

read(N) ->
	case string:to_float(N) of
		{error,no_float} -> list_to_integer(N);
		{F,_} -> F
	end.

rpn_test() ->
	5 = rpn("2 3 +"),
	87 = rpn("90 3 -"),
	-4 = rpn("10 4 3 + 2 * -"),
	-2.0 = rpn("10 4 3 + 2 * - 2 /"),
	ok = try
			 rpn("90 34 12 33 55 66 + * - +")
		 catch
			 error:{badmatch,[_|_]} -> ok
		 end,
	4037 = rpn("90 34 12 33 55 66 + * - + -"),
	8.0 = rpn("2 3 ^"),
	true = math:sqrt(2) == rpn("2 0.5 ^"),
	true = math:log(2.7) == rpn("2.7 ln"),
	true = math:log10(2.7) == rpn("2.7 log10"),
	50 = rpn("10 10 10 20 sum"),
	10.0 = rpn("10 10 10 20 sum 5 /"),
	1000.0 = rpn("10 10 20 0.5 prod"),
	ok.