-module(exception).
-export([condition/0,condition/1,condition_exception/1,list_exception/1,list_exception/0,justThrow/1,allExceptions/1,justThrowCustom/1]).

condition()-> 
	if dog == cat -> "meow..";
	    dog == dog -> "wow wow..";
		true -> "uknown"
	end.

condition(Animal)-> 
	if Animal == cat -> "meow..";
	    Animal == dog -> "wow wow..";
		true -> "uknown"
	end.

% it will generate exception
condition_exception(Animal)-> 
	if Animal == cat -> "meow..";
    Animal == dog -> "wow wow.."
	end.

% erlang:subtract([4],[2,4]). (valid example ) 
% erlang:subtract([4,2],[2]). (valid example )
% erlang:subtract(1,2). (exception )
% erlang:subtract([d],2). (exception ) 
subtract_exception(L1,L2) ->
	% it requires both should be list ( in other langauges we called in array)
	 erlang:subtract(L1,L1).

% examples will generate exceptions
% exception:list_exception(5).
% exception:list_exception("a , B, C, 4") (valid example)
% exception:list_exception("a"). (valid example)
% exception:list_exception(a).
% exception:list_exception([a,b,"A",5,"",'']). (valid example)
% exception:list_exception(a,b,c).
	% lists:sort(a,b,c). (error exception)
    % lists:sort(abc). (error exception)
% lists:sort("a,bc4").  (Valid example)
list_exception(L)-> lists:sort(L).

list_exception()-> 
	hd("a,b,c"), %(valid )
    tl("a,b,c"), %(valid )
    hd(abc) , % exception
    %hd(a,b,c) , % exception
    %tl(a,b,c), % exception

    io:fwrite("Function ended").

justThrow(Afunc)->
	try Afunc() of 
       _-> goodToGo
	catch throw:Ex -> {throw,caught,Ex}
	
	end.

justThrowCustom(Afunc)->
	try Afunc() of 
       _-> goodToGo
	catch throw:Ex ->  MSG = "Here is throw exception " ++ Ex, io:fwrite(MSG)
	
	end.

allExceptions(Afunc) ->
  try Afunc() of 
   	_-> "Good to Go"
  catch 
	throw:Th -> string:concat("Throw Case ", Th);
	error:Er -> string:concat("Error Case ", Er);
    exit:Exit -> "Exit Case " ++ Exit;
    _:_->  "default Case"
  end.
