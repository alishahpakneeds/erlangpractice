-module(records).
-compile(export_all). %compile all functions

-record(airplane,{manufacturer="default",make ="default 123",engines=[],year=2013}).
-record(user, {id, name, group, age}).

make_default()->
	#airplane{manufacturer="Boing",make=747,engines=["4 A33"],year = 2015}.

create_custom_plan(MF,Model,Engines,Year)-> 
	#airplane{manufacturer=MF,make=Model,engines=Engines,year = Year}.

get_plan_attributes() ->
	%records:make_default(),
	AirPlan1 = records:make_default(),
	AirPlan2 = records:create_custom_plan("Nasa","A3d3",["E2","E22"],2014),
	io:fwrite(AirPlan2#airplane.manufacturer ++ "\n"),
	io:fwrite(AirPlan2#airplane.make ++ "\n"),
	io:fwrite(AirPlan2#airplane.engines ++ "\n"),
	io:fwrite(integer_to_list(AirPlan2#airplane.year) ++ "\n"),
	io:fwrite("\n").

remodel(Plan) -> 
	P = records:make_default(),
	MAKE = integer_to_list(P#airplane.make),
	%Engines = Plan#airplane.engines,
	%Year = Plan#airplane.year,
	NewAirPlan = Plan#airplane{engines=["new engines"|Plan#airplane.engines],make=string:concat(MAKE,Plan#airplane.make)},
	{remodeled,NewAirPlan}.

execute_remodel()-> 
	records:remodel(#airplane{manufacturer="Ali Boy",year=2017,engines=["First engine","2nd Engine"],make = "ABC"}).

%------------------- User module -------------------%
%% Use pattern matching to filter.
admin_panel(#user{name=Name, group=admin}) ->
	Name ++ " is allowed!";
admin_panel(#user{name=Name}) ->
	Name ++ " is not allowed".
%% Can extend user without problem.
adult_section(U = #user{}) when U#user.age >= 18 ->
	%% Show stuff that can't be written in such a text.
	allowed;
adult_section(_) ->
	%% Redirect to Sesame Street site.
	forbidden.

set_user_record()-> 
	records:admin_panel(#user{id=1, name="ferd", group=admin, age=96}),
	records:admin_panel(#user{id=2, name="you", group=users, age=66}),
	records:adult_section(#user{id=21, name="Bill", group=users, age=72}),
	U = #user{id=22, name="Noah", group=users, age=13},
	records:adult_section(U),
	U .
	

repair_user(User)->
	U = set_user_record(),
	io:fwrite(U#user.group),
	UpdatedUser = User#user{age=25,group=U#user.group,id=U#user.id},
%% 	{repaird,UpdatedUser}.
    UpdatedUser.
execute_repair()-> 
	 records:repair_user(#user{name="ali"}).