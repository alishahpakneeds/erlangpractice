-module(syntax_tupple).
-compile(export_all).

print_elements({})->
	{};
	print_elements(T)->	
		if 
			tuple_size(T)>0 ->
				E = element(1, T),
				io:fwrite("~w~n",[E]),
				print_elements( erlang:delete_element(1, T));

%% 				try
%% 					 erlang:delete_element(E, T)
%% 				of
%% 					_-> Temp = erlang:delete_element(E, T), print_elements(Temp)
%% 				catch
%% 					throw:Th ->  io:fwrite("~w~n",[E]);
%% 				_:_->  ""	
%% 				end;
			true ->
				print_elements(T)
		end.
print_element_simple({})->
	{};
print_element_simple(T)->	
	E = element(1, T),
	io:fwrite("~w~n",[E]),
	print_element_simple( erlang:delete_element(1, T)).