%% @author aliabbas
%% @doc @todo Add description to hello_world.


-module(hello_world).

%% ====================================================================
%% API functions
%% ====================================================================
-export([start/1,say_hello/0,sorting_data/1,multiply/1,test/0,concat_error/1,concat/1,perform_concatination/1,atom_defination/0]).
-export([get_tupple_element/2,set_tupple_element/3,get_working_directory/0]).



test() -> 
   Fn = fun() -> 
      io:fwrite("Anonymous Function") end, 
   Fn().

start(X) -> 
   Sorting = fun(X) -> 
	  lists:sort(X),
	  io:fwrite(X) ,
      io:fwrite("In sorting") end,
   Sorting(X).

say_hello() -> io:fwrite("hello, world\n").

sorting_data(Y) -> lists:sort(Y).

multiply(X) -> X * 2.

concat_error(X) -> 
	C = "hello "+ X,
	io:frwrite(C).
	

concat(X) -> 
	if 
      is_integer(X) -> 
		 C = integer_to_list(X),
		 hello_world:concatination(C); 
      true -> 
          hello_world:concatination(X)
   end.

atom_defination()-> 
	io:fwrite(abc),
%% 	below is wrong you cannot assign differnt value in atom 
	abc = 'def',
	abc = 'abc',
    io:fwrite("\n").

%% ===========
%calling concat
perform_concatination(X)->
	C = string:concat("hello ", X),
	io:fwrite(C),
	io:fwrite("\n").
%%============= Calling functions %=============%%

%% -------------------------------------------
%% Tupples paractice
%---------------------------------------------


get_tupple_element(I,T)->
	element(I,T).
set_tupple_element(I,T,E)->
  	setelement(I,T,E).

get_working_directory() ->
%% 	X = pwd()
%% 	io:fwrite(pwd()),
	io:fwrite("\n").

%% e.g %%
%% Index will start from 1 not the zero 
%% TUP = {abc,def,ghi,jkl}




%% ====================================================================
%% Internal functions
%% ====================================================================

%% not equal comparision
%4.51=/=4.500.

% F = fun(3)->2*2.
