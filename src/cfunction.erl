-module(cfunction).
-compile(export_all).

one(X)-> X.
two()-> 2*9.

add(X,Y,N) -> X(N) + Y().

call_add()->
	add(fun one/1, fun two/0,1).

%---- function inside functions %%

inside_func(Room)->
	io:fwrite("I am inside ~s ~n",[Room]),
	fun()-> 
		io:fwrite("and , I am doing potti :D ~n.!") 
	end.
call_inside()->
	T = inside_func("Bathroom"),
	T().
	
a1() ->
	Secret = "pony",
	fun() -> Secret end.
b1(F) ->
	"a/0's password is "++F().