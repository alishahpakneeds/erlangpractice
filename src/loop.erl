-module(loop). 
-export([while/1,while/2, start/0,startFor/0,for_loop/3]). 

while(L) -> while(L,0). 
while([], Acc) -> Acc;
	while([H|T], Acc) ->
	   io:fwrite("~w~n",[H]), 
	   while(T,H). 
   
start() -> 
   X = [1,2,3,4,5,7], 
   while(X).

%=================================%
for(0,_) -> 
   []; 
   
   for(N,Term) when N > 0 -> 
   io:fwrite("Hello~n"), 
   [Term|for(N-1,Term)]. 
   
startFor() -> 
   for(5,1).

%==
for_loop(FROM,TO,[]) -> 
	[];
	for_loop (FROM,TO,L)->
		 if 
			 FROM =< TO ; TO < length(L) ->
				E = lists:nth(FROM,L),
		 		io:fwrite("~w~n",[E]),
			 	for_loop(FROM+1,TO,L);
			true->
%% 				for_loop(FROM,TO,L)
				[]
		end.
	  