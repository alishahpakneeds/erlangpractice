-module(mysql_connect).
-compile(export_all).
-define(CONNECTIONSTRING, "dsn=mysql_dsn;server=localhost;database=cjab_test;user=root;password=admin;").
get_connection_string()-> 
	"dsn=mysql_dsn;server=localhost;database=cjab_test;user=root;password=admin;".

mysql_connection()->
     application:stop(odbc),
case application:start(odbc) of
    ok->
        {ok, Ref}=odbc:connect(get_connection_string(),[]),
        Ref;
   
     {error,Reason}->
      	Reason
end.

get_data()->
	{ok, Conn}  = mysql_connection(),
	Results = odbc:sql_query(Conn, "SELECT * FROM psi_reports"),
	Results.