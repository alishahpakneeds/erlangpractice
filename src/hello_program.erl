-module(hello_program).

-export([test/0,hello/0]).

hello()-> 
	receive 
     {FromPID,Who} -> 
		  case Who of 
			  john -> FromPID ! "Hello John."; 
			  jim -> FromPID ! "Hello Jim.";
              ali -> FromPID ! "Hello Ali.";
			  _->FromPID! "Unknown selection."
	 end,
    hello()
end.

test()-> 
	io:fwrite("test.._").



%% Pid = spawn(fun hello_program:hello/0).
%% Pid ! john.
%% Pid ! {self(),john}.

%% Pid ! {self(),john},
%% receive
%% Response -> 
%% Response
%% end