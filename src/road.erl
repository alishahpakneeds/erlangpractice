-module(road).
-compile(export_all).
main() ->
	File = "hello_world/practice/test.csv",
	{ok, Bin} = file:read_file(File),
	parse_map(Bin).
main2() ->
	File = "hello_world/practice/test2.csv",
	{ok, Bin} = file:read_file(File),
	parse_map(Bin).

group_vals([], Acc) ->
	lists:reverse(Acc);
group_vals([A,B,X|Rest], Acc) ->
	group_vals(Rest, [{A,B,X} | Acc]).

parse_map(Bin) when is_binary(Bin) ->
	parse_map(binary_to_list(Bin));
parse_map(Str) when is_list(Str) ->
	F =  string:tokens(Str,"\n"),
%% 	T = ["ali","dd"],
	CsvList = [string:tokens(X,",") || X <- F],
	io:fwrite("\n"),
	io:fwrite(lists:last(CsvList)),
	io:fwrite("\n").
%% 	Values = [list_to_integer(X) || X <- string:tokens(Str,"\r\n\t ")],
%% 	group_vals(Values, []).


hello_world()->
	io:fwrite("Hello world \n").

hello_world2()->
    receive
         {From, case1} ->
            io:format("what you doing case 1 .~n");
         {From, case2} ->
            io:format("case 222 .~n");
        _ ->
            io:format("default case .~n")
    end.
	

hello_world3()->
	io:fwrite("Hello world 3 .~n").

hello_world4()->
	io:fwrite("Hello world 4 .~n").

execute_1()-> 
	
	Dolphin = spawn(road, main, []),
	Dolphin1 = spawn(road, main2, []),
	
	Dolphin2 = spawn(road, hello_world2, []),
	Dolphin2 ! {self(), case1},
	Dolphin3 = spawn(road, hello_world3, []),
	
	hello_world4().