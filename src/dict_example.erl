-module(dict_example).
-compile(export_all).


test()->
	M = #{a => 2, b => 3, c=> 4, "a" => 1, "b" => 2, "c" => 4},
	Pred = fun(K,V) -> is_atom(K) andalso (V rem 2) =:= 0 end,
	
	maps:filter(Pred,M).