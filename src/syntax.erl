-module(syntax).
-compile(export_all).

greet(male, Name) ->
	io:format("Hello, Mr. ~s!", [Name]);
greet(female, Name) ->
	io:format("Hello, Mrs. ~s!", [Name]);
greet(_, Name) ->
	io:format("Hello, ~s!", [Name]).

%% function greet(Gender,Name)
%% if Gender == male then
%% print("Hello, Mr. %s!", Name)
%% else if Gender == female then
%% print("Hello, Mrs. %s!", Name)
%% else
%% print("Hello, %s!", Name)
%% end

%---------------------%
print_odd_head([]) ->
	[];
print_odd_head([H|T])  when H rem 2 > 0 ->
	io:fwrite("~w~n",[H]), 
	print_odd_head(T).

%---------------------%	
get_odd_list([])->
	[];
get_odd_list([H|T])  when H rem 2 > 0 ->[H | get_odd_list(T)];
get_odd_list([H|T])  when H rem 2 == 0 -> T.

%---------------------%	
get_even_list([])->
	[];
get_even_list([H|T])  when H rem 2 == 0 ->[H | get_even_list(T)];
get_even_list([H|T])  when H rem 2 > 0 -> T.

get_custom_even_list(L)->
	if 
		length(L)==0 -> [];
		true -> 
			[H|T]=L,
			if
				H rem 2 == 0 -> 
					[H | get_custom_even_list(T)];
				true ->
					get_custom_even_list(T)
			end
	
	end.

get_custom_odd_list(L)-> if length(L)>0 -> get_custom_odd_list(L,0); true -> [] end.
get_custom_odd_list([],Acc)-> 
	[];
get_custom_odd_list([H|T], Acc) ->
	if
		H rem 2 =/= 0 -> 
			[H | get_custom_odd_list(T,H)];
		true ->
			get_custom_odd_list(T,0)
	end.



get_occurance1(L,N)-> length(get_occurance1(L,N,0)).
get_occurance1([],N,C)-> 
	[];
get_occurance1([H|T], N,C) when H == N  ->
	[H | get_occurance1(T,N,C)];
get_occurance1([H|T], N,C) when H =/= N  ->
	get_occurance1(T,N,C).

%% get_occurance(L,N)-> get_occurance(L,N,0)
%% 
%% get_occurance(L,N,C)->
%% 	C;
%% 	[H | T] ->  
%% 		if H == C ->
%% 			C = C + 1 ,
%% 			get_occurance(T,N,C);
%% 		true -> 
%% 			C
%% 		end.

len([]) -> 0; 
len([H|T])   -> 1 + len(T).
%----%
tail_len(L) -> tail_len(L,0).
 
tail_len([], Acc) -> Acc;
tail_len([H|T], Acc)  -> tail_len(T,Acc+1).

prepare_duplicate(0,_) ->
[];
prepare_duplicate(N,Term) when N > 0 ->
[Term|prepare_duplicate(N-1,Term)].

len1([],_) -> 0; 
len1([H|T],N) when  H==N -> 1 + len1(T,N);
len1(T,N) -> len1(T,N).


get_occurance([],N) -> 
	0;
get_occurance([H|T],N) ->
	if 
		H==N ->
			1 + get_occurance(T,N);
		true ->
			get_occurance(T,N)
	end.

get_duplicate_items([]) -> 
	[];
	get_duplicate_items(L)->
	    
		[H|T] = L,
		Check1 = get_occurance(L,H),
		io:fwrite("~w~n",[H]), 
		io:fwrite("~w~n",[Check1]), 
		io:fwrite("=====\n"),
		if 
			Check1 >1 -> 
				%simple_list:print_list(L),
				%io:fwrite("=====\n"),
				[H | get_duplicate_items(T)];
			true -> 
				get_duplicate_items(T)
		end.
			
get_unique_items([]) -> 
	[];
	get_unique_items(L)->
	    
		[H|T] = L,
		Check1 = get_occurance(L,H),
		io:fwrite("~w~n",[H]), 
		io:fwrite("~w~n",[Check1]), 
		io:fwrite("=====\n"),
		if 
			Check1 ==1 -> 
				[H] ++  get_unique_items(T);
			true -> 
				get_unique_items(T)
		end.							 
remove_repeated_items([])-> 
	[];
	remove_repeated_items(L)->
		[H|T] = L,
		Check1 = get_occurance(L,H),
		if 
			Check1 > 1 -> 
				 %it will remove the repeated item from array
				 remove_repeated_items(T) -- [H];
			true -> 
				[H] ++ remove_repeated_items(T)
		end.	
		
old_enough(X) when X >= 16 -> true;
old_enough(_) -> false.

heh_fine() ->
	if 1 =:= 1 ->
		   works
	end,
	if 1 =:= 2; 1 =:= 1 ->
		   works
	end,
	if 1 =:= 2, 1 =:= 1 ->
		   fails
	end.
help_case(Temperature) ->
	case Temperature of 
		{celsius,N} when N > 31 ; N< 36 ->
			"Normal";
		{kelvin, N} when N >= 293, N =< 318 ->
			'scientifically favorable';
		{fahrenheit, N} when N >= 68, N =< 113 ->
			'favorable in the US';
		_->
			"Default"
	end.
case_fact(N)-> 
	case N of 
		N when N>1 -> 
			N*case_fact(N-1);
		N when N == 1 ->
			1;
	_->
		0
	end.	