-module(dolphins).
-compile(export_all).

dolphin1() ->
    receive
        do_a_flip ->
            io:format("How about no?~n");
        fish ->
            io:format("So long and thanks for all the fish!~n");
        _ ->
            io:format("Heh, we're smarter than you humans.~n")
    end.

dolphin2() ->
    receive
        {From, do_a_flip} ->
            From ! "How about no?";
        {From, fish} ->
            From ! "So long and thanks for all the fish!";
        _ ->
            io:format("Heh, we're smarter than you humans.~n")
    end.

dolphin3() ->
    receive
        {From, do_a_flip} ->
            From ! "How about no?",
            dolphin3();
        {From, fish} ->
            From ! "So long and thanks for all the fish!";
        _ ->
            io:format("Heh, we're smarter than you humans.~n"),
            dolphin3()
    end.

hello_world()->
	io:fwrite("Hello world \n").

hello_world2()->
	io:fwrite("Hello world 2 \n").

hello_world3()->
	io:fwrite("Hello world 3 \n").

hello_world4()->
	io:fwrite("Hello world 4 \n").

execute_1()-> 
	
	Dolphin = spawn(dolphins, hello_world, []),
	Dolphin2 = spawn(dolphins, hello_world2, []),
	Dolphin3 = spawn(dolphins, hello_world3, []),
%% 	Dolphin3 = spawn(dolphins, hello_world3, []),
	hello_world4().
	
%% 	io:fwrite("done \n").
	
%% 	Dolphin = spawn(dolphins, dolphin1, []),
%% %% 	Dolphin ! "oh, hello dolphin!",
%% 	Dolphin ! fish,
%%     Dolphin.
execute_2()-> 
	Dolphin2 = spawn(dolphins, dolphin2, []),
	Dolphin2 ! {self(), do_a_flip},
    Dolphin2.
execute_3()-> 
	Dolphin3 = spawn(dolphins, dolphin3, []),
	Dolphin3 ! Dolphin3 ! {self(), do_a_flip},
	Dolphin3 ! {self(), unknown_message},
	Dolphin3 ! Dolphin3 ! {self(), fish},
    Dolphin3.