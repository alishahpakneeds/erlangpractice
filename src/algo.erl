%% @author aliabbas
%% @doc @todo Add description to algo.


-module(algo).
-export([fact/1,get_factorial/1,table/2,table_single/1]).

%-----------------Factorial ----------------%

fact(N) when N == 0 -> 1; 
	fact(N) when N > 0 -> N*fact(N-1). 

get_factorial(N)->
	X = fact(N),
	
	io:fwrite("~w",[X]),
	io:fwrite("\n").
   
table(N,S) ->
	if 
		S=<9 ->
			 table(N,S+1);
		true ->
		 io:fwrite("\n")
	end, 
    T = string:concat(string:join([integer_to_list(N), integer_to_list(S)], "X"),"="),
	io:fwrite(T),
	io:fwrite("~w\n",[N*S]).
	
table_single(N) ->
	S = N - (N - 1),
	if 
		S=<10 ->
%% 			 table_single(N-1);
			S = N - (N - 1),
			%table(N,S),
			io:fwrite("");
		true ->
		 io:fwrite("\n")
	end, 
    T = string:concat(string:join([integer_to_list(N), integer_to_list(S)], "X"),"="),
	io:fwrite(T),
	io:fwrite("~w",[N*S]),
	io:fwrite("\n").