%% @author aliabbas

-module(list_demo).

-export([unique/1,unique_merge/2,manipulate_tupple_list/0,manipulate_list/0,subtract_list/0,getCitiesByCountry/1,getCountryByCityName/1]).
-export([select_all/2,select/2]).

unique(L)-> sets:to_list(sets:from_list(L)).
unique_merge(L1,L2) ->
  lists:sort(sets:to_list(sets:from_list((L1 ++ L2)))).

manipulate_tupple_list()->
	L = [{hwy12,45},{hwy67,55},{hwy112,65},{hwy125,75}],
	N = [{H,Speed - 5} || {H,Speed} <- L,Speed>=55,Speed=<74].
	

manipulate_list()->
	M = [1,3,4,6],
	%N = [H*2 || H <- M,H >1],
    S = [H+1 || H <- M,H >1 , H < 5].

subtract_list()->
	X = [5,10],
    Y = [10,15],
%% 	[A - B || A <- X,B <- Y].
%% 	[A - B || A <- X,B <- Y,A > 5].
	[A - B || A <- X,B <- Y,A > 5,B<15].

getCitiesByCountry(CountryName)->
	CityLoc   =  [{lahore,pak},{karachi,pak},{mumbai,ind},{dhaka,ban}],
	%another simple method but it will work with hard coded not from the parameter
   	%Cities  = [City || {City,pak} <- CityLoc]. === (Wrong)
	%Cities  = [City || {City,CountryName} <- CityLoc]. === (Right)
    %-------------------------------------------------------------%
	Cities = [City || {City,Country} <- CityLoc, Country == CountryName].
	
getCountryByCityName(CityName)->
	CityLoc   =  [{lahore,pak},{karachi,pak},{mumbai,ind},{dhaka,ban}],
	%FoundCountry = [Country || {City,Country} <- CityLoc, City == CityName],
    
    %another simple way by hardcoded
    %FoundCountry1 = [Country || {dhaka,Country} <- CityLoc,CityName],

    %another way 
    FoundCountry2 = list_demo:select(CityName,CityLoc).
    

%utiltiy methods
select_all(X, L) -> [Y || {X, Y} <- L]. % this will return all 
select(X, LOC) ->  [Y || {X1, Y} <- LOC, X == X1]. % this will match

